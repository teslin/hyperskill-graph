
const relations = new Relations("#svg1", "#svgContainer");

function resetSVGsize(){
  relations.resetPan();
}

function connectAll() {
  relations.connect($("#topic_1"), $("#topic_2"), $("#path1"));
  relations.connect($("#topic_2"), $("#topic_3"), $("#path2"));
  relations.connect($("#topic_a"), $("#topic_b"), $("#path3"));
  relations.connect($("#topic_c1"), $("#topic_e"), $("#path4"));
}

function redraw() {
  //resetSVGsize();
  connectAll();
}





$(document).ready(function() {
  setTimeout(()=>redraw(), 100);

  $("#topic_1,#topic_2,#topic_3,#supergroup2").draggable({
    drag: function() {
      redraw();
    }
  });

  $('.graph-topic').on('click', function () {
    $(this).toggleClass('graph-topic_open');
    redraw();
  });

  $('.graph-group .graph-group__header').on('click', function () {
    $(this).parent('.graph-group').toggleClass('graph-group_active');
  });
});

$(window).resize(function () {
  redraw();
});