
class Relations {

  /**
   * @orientation_rules
   *    Rules defining the orientation of relationships
   *    vertical: { at, ac, ab, ah, bt, bc, bb, bh }
   *    horizontal: { al, ac, ar, aw, bl, bc, br, bw }
   */
  constructor(svg, svgContainer, orientation_rules={}) {
    const dafault_orientation_rules = {
      vertical: {
        center: ({ac, bt, bb}) => ac > bt && ac < bb,
        bottom: ({}) => true,
      },
      horizontal: {
        left: ({al, br}) => (al-br) > 20,
        left_center: ({ac, br}) => ac > br,
        center: false,//({ac, bl, br}) => ac > bl && ac < br,
        right: ({ar, bl}) => (bl-ar) > 20,
        right_center: ({ac, bl}) => ac < bl,
        default: 'right_center'
      }
    };

    this.svg = svg;
    this.container = svgContainer;
    this.rules = Object.assign({}, dafault_orientation_rules, orientation_rules);
    this.defaultDelta = 5;
  }

  connect(startElem, endElem, $path) {
    const { $startElem: $first, $endElem: $second } = this._replaceElements(startElem, endElem);
    const orientation = this._computeOrientation($first, $second);
    const coordinates = this._getCoordinatesRelation($first, $second, orientation);
    const { start, end } = this._correctCoordinates(coordinates);

    //this.resizePan($path, 0, 0, 1000, 1000);
    this.drawPath($path, start, end, orientation, 6);
  }

  drawPath($path, start, end, {vertical, horizontal}, offset) {
    const delta = this._getDelta(start, end);
    let line;

    if ((horizontal === 'left' || horizontal === 'right') && vertical === 'center') {
      line = [
        ['M', start[0] + " " + start[1]],
        ['H', end[0]]
      ];
    } else if (horizontal === 'center' && vertical === 'bottom') {
      line = [
        ['M', start[0] + " " + start[1]],
        ['V', end[1]]
      ];
    } else if (horizontal === 'right_center' && vertical === 'bottom') {
      line = [
        ['M', start[0] + " " + start[1]],
        ['H', end[0] + offset],
        ['A', this._getSvgArch(delta, 1, end[0] + offset + delta, start[1] + delta)],
        ['V', end[1] - delta],
        ['A', this._getSvgArch(delta, 1, end[0] + offset, end[1])],
        ['H', end[0]],
      ];
    } else if (horizontal === 'right' && vertical === 'bottom') {
      line = [
        ['M', start[0] + " " + start[1]],
        ['H', start[0] + offset],
        ['A', this._getSvgArch(delta, 1, start[0] + offset + delta, start[1] + delta)],
        ['V', end[1] - delta],
        ['A', this._getSvgArch(delta, 0, start[0] + offset + 2*delta, end[1])],
        ['H', end[0]],
      ];
    } else if (horizontal === 'left_center' && vertical === 'bottom') {
      line = [
        ['M', start[0] + " " + start[1]],
        ['H', end[0] - offset],
        ['A', this._getSvgArch(delta, 0, end[0] - offset - delta, start[1] + delta)],
        ['V', end[1] - delta],
        ['A', this._getSvgArch(delta, 0, end[0] - offset, end[1])],
        ['H', end[0]],
      ];
    } else if (horizontal === 'left' && vertical === 'bottom') {
      line = [
        ['M', start[0] + " " + start[1]],
        ['H', start[0] - offset],
        ['A', this._getSvgArch(delta, 0, start[0] - offset - delta, start[1] + delta)],
        ['V', end[1] - delta],
        ['A', this._getSvgArch(delta, 1, start[0] - offset - 2*delta, end[1])],
        ['H', end[0]],
      ];
    } else {
      line = [];
    }

    const line_str = line.map(item => Object.values(item).join('')).join(' ');
    $path.attr("d", line_str);
  }

  resizePan(path, startX, startY, endX, endY) {
    const $svg = $(this.svg);
    $svg
    .attr("height", endY)
    .attr("width", endX);
  }

  resetPan(){
    const $svg = $(this.svg);
    $svg
    .attr("height", "0")
    .attr("width", "0");
  }

  _getDelta() {
    return this.defaultDelta;
  }

  _getSvgArch(radius, arc, x, y) {
    const a = {
      rx: radius,
      ry: radius,
      x_axis_rotation: 0,
      large_arc_flag: 0,
      sweep_flag: arc,
      x: x,
      y: y
    };
    return Object.values(a).join(' ');
  }

  _getPosition($elem) {
    const top = $elem.offset().top;
    const left = $elem.offset().left;
    const height = $elem.outerHeight();
    const width = $elem.outerWidth();
    const centerX = left + 0.5*width;
    const centerY = top + 0.5*height;
    const right = left + width;
    const bottom = top + height;

    return { top, left, height, width, bottom, right, centerX, centerY };
  }

  _replaceElements($startElem, $endElem) {
    const { centerY: startCenterY } = this._getPosition($startElem);
    const { centerY: endCenterY } = this._getPosition($endElem);

    return startCenterY > endCenterY
      ? { $startElem: $endElem, $endElem:  $startElem }
      : { $startElem, $endElem };
  }

  _computeOrientation($a, $b) {
    const aPos = this._getPosition($a);
    const bPos = this._getPosition($b);
    const paramsSet = {
      vertical: {
        at: aPos.top,
        ac: aPos.centerY,
        ab: aPos.bottom,
        ah: aPos.height,
        bt: bPos.top,
        bc: bPos.centerY,
        bb: bPos.bottom,
        bh: bPos.height
      },
      horizontal: {
        al: aPos.left,
        ac: aPos.centerX,
        ar: aPos.right,
        aw: aPos.width,
        bl: bPos.left,
        bc: bPos.centerX,
        br: bPos.right,
        bw: bPos.width
      }
    };
    const rules = this.rules;

    return Object.keys(rules)
      .map(direction => {
        const params = paramsSet[direction];
        const variants = Object.keys(rules[direction]);
        const rule = rules[direction];
        const found = variants.find(variant =>
          rule[variant]===true
          || (typeof rule[variant] === 'function' && rule[variant](params))
        );
        const value = found || rule.default || null;
        return [ direction, value ];
      })
      .reduce(this._objectify, {});
  }

  _getCoordinatesRelation($a, $b, {vertical, horizontal}) {
    const aPos = this._getPosition($a);
    const bPos = this._getPosition($b);

    let start;
    let end;

    if (horizontal === 'left') {
      start = [ aPos.left, aPos.centerY ];
      end = [ bPos.right, bPos.centerY ];

    } else if(horizontal === 'left_center') {
      start = [ aPos.left, aPos.centerY ];
      end = [ bPos.left, bPos.centerY ];

    } else if (horizontal === 'right') {
      start = [ aPos.right, aPos.centerY ];
      end = [ bPos.left, bPos.centerY ];

    } else if (horizontal === 'right_center') {
      start = [ aPos.right, aPos.centerY ];
      end = [ bPos.right, bPos.centerY ];

    } else if (horizontal === 'center') {
      start = [ aPos.centerX, aPos.bottom ];
      end = [ bPos.centerX, bPos.top ];
    }

    return { start, end };
  }

  _correctCoordinates(coords) {
    const $container = $(this.container);

    if (!coords || !$container) {
      return coords;
    }

    const offset = [ $container.offset().left, $container.offset().top ];

    const result = Object.keys(coords)
      .map(name => {
        const values = coords[name].map((value, index) => value - offset[index]);
        return [name, values];
      })
      .reduce(this._objectify, {});

    return result;
  }

  _objectify(obj, [k, v]) {
    return { ...obj, [k]: v }
  }
}
